This project aims to simplify reconfiguring embedded home automation devices after you hide them in your home.  
With the c / arduino based projects you need to recompile and upload the new firmware, with micropython your runtime stays the same and you just upload the new code to your embedded device.


This is a work in progress and at the moment more of an inspiration for your own experiments.

How to use:
 - setup a microcontroler with [micropython](https://micropython.org/download)
 - install mpfshell (if you have trouble connecting try downgrading the websocket module)
 - edit `boot.py` and fill in your wifi credentials.
 - make a private copy of the wifi config `cp wifi-config.json.sample wifi-config.json`, then edit `wifi-config.json` and enter your wifi credentials
 - edit `config.json` and configure your mqtt host, topic and the pin you want as an output.
 - edit `webrepl_cfg.py` change password to a 4 - 8 char password, I had issues with complex passwords.
 - open mpfshell over local serial and upload the files
 - now you can reach it over the network after restarting your microcontroler

## How to flash sonoff basic with micropython

You'll need to install esptool.py(`pip install esptool`) and you might want to adfd yourself to the dialout group (`sudo usermod -a -G dialout ${USERNAME}`) and download the latest stable 1mb [micropython](https://micropython.org/download/esp8266/) release for esp8266.

### erase
- Hold button and plug in usb serial to start in config mode
- run: `esptool.py --port /dev/ttyUSB0 erase_flash`
- wait till completed and a few extra seconds just to be sure
- Remove usb cable from pc

### flash
- Hold button and plug in usb serial to start in config mode
- run: `esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect -fm dout 0x0 ~/Downloads/esp8266-1m-20200902-v1.13.bin`
- Wait till completed and about 30 seconds extra
- Remove usb cable from pc

### verify
- plug in usb serial to start in normal mode
- run: `screen /dev/ttyUSB0 115200`
- hit enter to start micropython repl, you should see `>>>` run `help()` to verify that it's working.
- Close by disconnecting usb serial or Ctrl + a k

### upload code to the sonoff
After flashing I like using mpfshell to manage the files on the device, first locally to configure wifi and later OTA 

## Config

### Sonoff basic
```
{
    "mqtt":{
        "host":"192.168.1.3"
    },
    "sensors":[],
    "outputs":[
	{
        "pin":"12",
        "type":"switch",
        "invert": "False",
        "topic":"home/office/test"
	}
    ]
}
```

## Test mqtt messages
 > Topic: `home/office/test`

```
{"state": "TOGGLE"}
```
You can hear the relay even if you're just on the 3.3v usb serial power.


![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fmicropython-home-assistant%2FREADME.md&dt=micropython-home-assistant)

