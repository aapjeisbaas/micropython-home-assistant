from umqtt.simple import MQTTClient
from machine import Pin
from machine import Timer
import ubinascii
import machine
import micropython
import json
import onewire
import ds18x20
import time

with open('config.json') as json_data_file:
    cfg = json.load(json_data_file)

# Default MQTT server to connect to
SERVER = cfg['mqtt']['host']
try:
    SERVER_PORT = int(cfg['mqtt']['port'])
except:
    SERVER_PORT = 1883
CLIENT_ID = ubinascii.hexlify(machine.unique_id())

# timer for scheduled sensor reading
tim = Timer(-1)

def sub_cb(topic, msg):
    topic = topic.decode("utf-8")
    msg = msg.decode("utf-8")
    output = [ output for output in cfg['outputs'] if topic in output['topic']][0]
    output_pin = output['output_pin']
    if output['invert'].lower() == 'true':
        on_value = 0
        off_value = 1
    else:
        on_value = 1
        off_value = 0

    try:
        state = json.loads(msg)['state']
        if state == "ON":
            output_pin.value(on_value)
        elif state == "OFF":
            output_pin.value(off_value)
        elif state == "TOGGLE":
            output_pin.value(not output_pin.value())
    except:
        print('Invalid json or payload')

class Sensor(object):
    def __init__(self, interval, pin, sensor_type, topic, mqttClient):
        self.interval = int(interval)
        self.pin = machine.Pin(int(pin))
        self.sensor_type = sensor_type
        self.topic = topic
        self.mqttClient = mqttClient

        if self.sensor_type.lower() == 'ds18b20':
            self.ds = ds18x20.DS18X20(onewire.OneWire(self.pin))
            self.rom = self.ds.scan()[0]

        if self.interval > 0:
            tim.init(period=self.interval, mode=Timer.PERIODIC, callback=self.cb)
        else:
            # pin rise / pin fall
            p0 = Pin(0, Pin.IN)
            p0.irq(trigger=Pin.IRQ_RISING, handler=self.pin_cb)

    def cb(self, tim):
        if self.sensor_type.lower() == 'ds18b20':
            self.ds18b20_cb()

    def pin_cb(self, x):
        self.msg = 'ON'
        self.mqttClient.publish(self.topic, self.msg)
        time.sleep_ms(500)
        self.msg = 'OFF'
        self.mqttClient.publish(self.topic, self.msg)

    def ds18b20_cb(self):
        self.ds.convert_temp()
        time.sleep_ms(750)
        self.temp = self.ds.read_temp(self.rom)
        self.msg = '{"temperature":' + str(self.temp) + '}'
        self.mqttClient.publish(self.topic, self.msg)


def main(server=SERVER):
    mqttClient = MQTTClient(CLIENT_ID, server, port=SERVER_PORT)
    # Subscribed messages will be delivered to this callbacuk
    mqttClient.set_callback(sub_cb)
    mqttClient.connect()

    for sensor in cfg['sensors']:
        try:
            Sensor(sensor['interval'], sensor['pin'], sensor['type'], sensor['topic'], mqttClient)
        except:
            print("sensor did not respond")

    for output in cfg['outputs']:
        if output['invert'].lower() == 'true':
            default_value = 1
        else:
            default_value = 0
        output['output_pin'] = Pin(int(output['pin']), Pin.OUT, value=default_value)
        mqttClient.subscribe(output['topic'])
        print("Connected to %s, subscribed to %s topic" % (server, output['topic']))

    try:
        while 1:
            #micropython.mem_info()
            mqttClient.wait_msg()
    finally:
        mqttClient.disconnect()

main()
