# This file is executed on every boot (including wake-boot from deepsleep)

import machine
import time
import json
import network
import gc
gc.collect()

def no_debug():
    import esp
    esp.osdebug(None)

no_debug()

def do_connect():
    # load the wifi configuration
    with open('wifi-config.json') as json_data_file:
        cfg = json.load(json_data_file)

    # Connect to WIFI
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(str.encode(cfg['ssid']), str.encode(cfg['password']))
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())

def do_set_rtc_time():
    from ntptime import settime
    settime() # Use NTP to set clock
    print('Updated UTC time is:', time.localtime())

def do_start_webrepl():
    import webrepl
    webrepl.start()

try:
    do_connect()
    do_set_rtc_time()

except:
    time.sleep(20)
    machine.reset()

do_start_webrepl()
